﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRUD.Web.Models
{
    public class CadastroViewModel
    {
        public long Id { get; set; }

        public string NomeCompleto { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }
    }
}