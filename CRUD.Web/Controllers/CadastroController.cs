﻿using CRUD.Web.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CRUD.Web.Controllers
{
    public class CadastroController : Controller
    {
        #region Read
        // GET: Read
        public ActionResult Read()
        {
            var client = new RestClient("http://localhost:7307/api/cadastro");

            var response = client.Execute(new RestRequest());

            var modelList = JsonConvert.DeserializeObject<List<CadastroViewModel>>(response.Content);

            return View(modelList);
        }
        #endregion

        #region Create
        // GET: Create
        [HttpGet]
        public ActionResult Create()
        {
            var model = new CadastroViewModel();

            return View(model);
        }
        

        // Post: Create
        [HttpPost]
        public ActionResult Create(CadastroViewModel cadastro)
        {
            var client = new RestClient("http://localhost:7307/api/cadastro");
            var request = new RestRequest(Method.POST);

            var jsonCadastro = JsonConvert.SerializeObject(cadastro);

            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json; charset=utf-8", jsonCadastro, ParameterType.RequestBody);

            var response = client.Execute(request);

            return RedirectToAction("Read");
        }
        #endregion

        #region Delete
        // GET: Delete
        [HttpGet]
        public ActionResult Delete(long id)
        {
            var client = new RestClient("http://localhost:7307/api/cadastro/"+id);

            var response = client.Execute(new RestRequest());

            var modelList = JsonConvert.DeserializeObject<CadastroViewModel>(response.Content);

            return View(modelList);
        }

        // Post: Delete
        [HttpPost]
        public ActionResult Delete(CadastroViewModel cadastro)
        {
            var client = new RestClient("http://localhost:7307/api/cadastro/" + cadastro.Id);
            var request = new RestRequest(Method.DELETE);
            
            var response = client.Execute(request);

            return RedirectToAction("Read");
        }
        #endregion

        #region Update
        // GET: Update
        [HttpGet]
        public ActionResult Update(long id)
        {
            var client = new RestClient("http://localhost:7307/api/cadastro/" + id);

            var response = client.Execute(new RestRequest());

            var modelList = JsonConvert.DeserializeObject<CadastroViewModel>(response.Content);

            return View(modelList);
        }

        // Post: Update
        [HttpPost]
        public ActionResult Update(CadastroViewModel cadastro)
        {
            var client = new RestClient("http://localhost:7307/api/cadastro");
            var request = new RestRequest(Method.PUT);
            var jsonCadastro = JsonConvert.SerializeObject(cadastro);

            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json; charset=utf-8", jsonCadastro, ParameterType.RequestBody);

            var response = client.Execute(request);

            return RedirectToAction("Read");
        }
        #endregion
    }
}