﻿using CRUD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Domain.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("name=Database")
        {
            // the terrible hack
            var ensureDLLIsCopied =
                    System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // modelBuilder.Entity<Eventos>().Ignore(t => t.Id);
            Database.SetInitializer<ApplicationDbContext>(null);
        }
        public DbSet<Cadastro> Cadastro { get; set; }
    }
}
