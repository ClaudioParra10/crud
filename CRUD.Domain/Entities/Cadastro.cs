﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Domain.Entities
{
    [Table("CADASTRO")]
    public class Cadastro : BaseEntity
    {
        [Required]
        public string NomeCompleto {get;set;}
        [Required]
        public string Email {get;set;}
        [Required]
        public string Telefone { get; set; }
    }
}
