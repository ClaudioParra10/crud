﻿using CRUD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Domain.Interfaces
{
    public interface IService<T> where T : BaseEntity
    {
        T Post<V>(T obj);

        T Put<V>(T obj);

        void Delete(long id);

        T Get(long id);

        IList<T> Get();
    }
}
