﻿using CRUD.Domain.Context;
using CRUD.Domain.Entities;
using CRUD.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Domain.Repository
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private ApplicationDbContext _context = new ApplicationDbContext();

        public void Insert(T obj)
        {
            _context.Set<T>().Add(obj);
            _context.SaveChanges();
        }

        public void Remove(long id)
        {
            _context.Set<T>().Remove(Select(id));
            _context.SaveChanges();
        }
        public void Update(T obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public T Select(long id)
        {
            return _context.Set<T>().Find(id);
        }

        public IList<T> SelectAll()
        {
            return _context.Set<T>().ToList();
        }


    }
}
