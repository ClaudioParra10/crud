﻿using CRUD.Domain.Entities;
using CRUD.Domain.Interfaces;
using CRUD.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD.Application.Services
{
    public class BaseService<T> : IService<T> where T : BaseEntity
    {
        private BaseRepository<T> _repository = new BaseRepository<T>();

        public T Post<V>(T obj)
        {
            _repository.Insert(obj);
            return obj;
        }

        public T Put<V>(T obj)
        {
            _repository.Update(obj);
            return obj;
        }

        public void Delete(long id)
        {
            _repository.Remove(id);
        }

        public T Get(long id)
        {
            return _repository.Select(id);
        }

        public IList<T> Get()
        {
            return _repository.SelectAll();
        }
    }
}
