﻿using CRUD.Application.Services;
using CRUD.Domain.Entities;
using System.Web.Http;

namespace CRUD.Api.Controllers
{
    public class CadastroController : ApiController
    {
        BaseService<Cadastro> _services = new BaseService<Cadastro>();

        [HttpGet]
        public IHttpActionResult Get()
        {
            return Ok(_services.Get());
        }

        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            var model = _services.Get(id);

            if (model == null)
            {
                return NotFound();
            }

            return Ok(_services.Get(id));
        }


        [HttpPost]
        public IHttpActionResult Post([FromBody] Cadastro cadastro)
        {
            if (!ModelState.IsValid) { 
                return BadRequest();
            }

            return Ok(_services.Post<Cadastro>(cadastro));
        }


        [HttpPut]
        public IHttpActionResult Put([FromBody] Cadastro cadastro)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            return Ok(_services.Put<Cadastro>(cadastro));
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            var model = _services.Get(id);

            if (model == null)
            {
                return NotFound();
            }

            _services.Delete(id);
            return Ok();
        }

    }
}
